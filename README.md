## Guess The Number

This project is a simple React Native application based on an interactive game.
The goal is to familiarize with components, layout and stilying as provided by
a practical RN course you can find [here](https://github.com/academind/react-native-practical-guide-code).
